<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::group(['middleware' => ['auth:api']], function ($group) {

// List articles
// Route::get('/elements', 'ElementController@index');

// Create new element 
// Route::post('element/create', 'Api\ElementController@create');

// List single article 
// Route::get('element/{element_id}', 'Api\ElementController@show');

// Edit element 
// Route::post('element/{id}/edit', 'ElementController@edit');

// Almacena element 
// Route::post('/element/guardar', 'ElementController@store');

// Esta funciona Aqui Almacena element 
// Route::put('/element/{id}', 'ElementController@update');

// Delete element 
// Route::delete('/element/{id}', 'ElementController@destroy');

// });

// Route::post('/login', 'Api\LoginController@login');
