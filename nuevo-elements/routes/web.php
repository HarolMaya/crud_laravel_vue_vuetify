<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/notas', 'NotaController')->middleware('auth');

// Route::group(['middleware' => ['auth:api']], function ($group) {

// List articles
Route::get('/elements', 'ElementController@index');

// Create new element 
// Route::post('element/create', 'Api\ElementController@create');

// List single article 
// Route::get('element/{element_id}', 'Api\ElementController@show');

// Edit element 
// Route::post('element/{element_id}/edit', 'Api\ElementController@edit');

// Almacena element 
Route::post('/element/guardar', 'ElementController@store');

// Esta funciona aqui Almacena element 
Route::put('/element/{id}', 'ElementController@update');

// Delete element 
Route::delete('/element/{id}', 'ElementController@destroy');


// });

// Route::post('/login', 'Api\LoginController@login');
