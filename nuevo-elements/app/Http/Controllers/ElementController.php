<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Element;              //revisar
use App\Http\Resources\Element as ElementResources;

class ElementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $elements = Element::all();

        // return view('elements.index', compact('elements'));
        return ElementResources::collection($elements);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('element.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $element = new Element;
        // $element->id = $request->input('id'); // $element (es la variable) ->title (es el nombre de la columna) = $request->input('title')
        $element->title = $request->input('title');
        $element->content = $request->input('content');

        $element->save();

        // return redirect('/elements/' . $element->id);

        // if($element->save()) {
        //     return new ElementResource($element);
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($element_id)
    {
        $element = Element::findOrFail($element_id);
        // return view('elements.show', compact('element'));
        return new ElementResource($element);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($element_id)
    {
        $element = Element::findOrFail($element_id);
        
        return view('element.edit', compact('element'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $element = Element::findOrFail($id); // usa la identificacion para hacer las modificaciones de los inputs

        $element->title = $request->input('title'); // $element (es la variable) ->title (es el nombre de la columna) = $request->input('title')
        $element->content = $request->input('content');

        $element->save();

        return $element;
        //Esta función actualizará la tarea que hayamos seleccionado
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request )
    {
        // Get a single article

        $element = Element::destroy($id);

        $id = $request->input('id');
        
        return $element;

        // Return single article as a resource

        // if($element->delete()) {
        //    return new ElementResource($element);
        // }
    }
}
