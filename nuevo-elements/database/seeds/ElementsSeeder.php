<?php

use Illuminate\Database\Seeder;
use App\Element;

class ElementsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = json_decode( file_get_contents( storage_path('elements.json') ) );

        \DB::statement('TRUNCATE TABLE `elements`');

        foreach ($data as $element) {
            $new_element = new Element;
            $new_element->title     = $element->title;
            $new_element->content   = $element->content;

            $new_element->save();
        }
    }
}
